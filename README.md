# ECPay_with_flask

flask 串接 綠界金流 範例

## 安裝所需套件
pip install -r requirements.txt 

## 安裝ngrok軟體
- 官網：https://ngrok.com/
- windows:
    到官方網站下載，解壓縮後即可使用
- ubuntu:
    到官方網站下載ngrok.zip
    unzip /path/to/ngrok.zip


## ubuntu執行方法
1. 打開終端機，執行ngrok http 5000
2. 複製ngrok產生的網址（例如：https://031f-2001-b400-e25d-2f05-ef62-c8b9-393b-cb78.ngrok.io）
3. 打開config.py
4. 將第7行的BASE_URL值改為第2步取得的網址，存檔
5. 打開另外一個終端機，執行make up
6. 點http://127.0.0.1:5000

## windows執行方法
1. 打開終端機，執行ngrok http 5000
2. 複製ngrok產生的網址（例如：https://031f-2001-b400-e25d-2f05-ef62-c8b9-393b-cb78.ngrok.io）
3. 打開config.py
4. 將第7行的BASE_URL值改為第2步取得的網址，存檔
5. 打開另外一個終端機，執行
    a. set FLASK_APP=app
	b. set FLASK_ENV=development
	c. flask run --reload --debugger
6. 點http://127.0.0.1:5000