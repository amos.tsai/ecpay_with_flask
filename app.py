import os
from flask import Flask, render_template

from extensions import db, csrf
from blueprints.payment.controllers import payment

def create_app():
    env = os.environ.get('FLASK_ENV', 'product')

    app = Flask(__name__, static_folder='static')
    app.url_map.strict_slashes = False
    app.config.from_object('config.%sConfig' % env.capitalize())

    register_extensions(app)
    register_blueprints(app)
    register_baseroute(app)
    return app


def register_extensions(app):
    """
    註冊第三方套件

    :param app: Flask application instance
    :return: None
    """
    # mail.init_app(app)
    db.init_app(app)
    csrf.init_app(app)


def register_blueprints(app):
    '''
    註冊各功能blueprints

    :param app
    :return None
    '''
    app.register_blueprint(payment)


def register_baseroute(app):
    @app.route('/', methods=['GET'])
    def index():
        return render_template('cart.html')

    @app.route('/init_DB')
    def initDB():
        from blueprints.payment.model import Payment

        db.create_all()
        db.session.commit()
        return '<div>資料庫建立成功</div>'

    @app.route('/url_map')  # TODO: 正式上線時要註解掉！
    def show_urlmap():
        print(app.url_map)
        return ('', 200)