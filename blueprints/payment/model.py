from extensions import db

class Payment(db.Model):
    __tablename__ = 'payment'

    uid = db.Column(db.Integer(), primary_key=True, autoincrement=True)
    tid = db.Column(db.Integer())
    trade_name = db.Column(db.String(50))
    trade_phone = db.Column(db.String(15))
    address = db.Column(db.String(200))
    total_product_price = db.Column(db.Integer())
    status = db.Column(db.String(100))
    county = db.Column(db.String(20))
    district = db.Column(db.String(20))
    zipcode =db.Column(db.String(10))

    def __init__(self, **kwargs):
        super(Payment, self).__init__(**kwargs)

    def __repr__(self):
        return f"<Payment: {self.uid} {self.trade_name}'>"