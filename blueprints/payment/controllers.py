import os, json, time
from datetime import datetime
import importlib.util
from flask import Blueprint, render_template, request, redirect, url_for, current_app
from extensions import db, csrf

payment = Blueprint('payment', __name__, url_prefix='/payment', static_folder='static', template_folder='templates')


def import_ecpay_module():
    '''動態載入ecpay模組'''
    path = current_app.config['ROOT_DIR']
    spec = importlib.util.spec_from_file_location(
        "ecpay_payment_sdk",
        path + '/lib/ecpay_payment_sdk.py'
    )
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)
    return module


# 建立訂單後跳轉至 ECpay 頁面
@payment.route('/to_ecpay', methods=['GET', 'POST'])
def ecpay():
    # method = request.method
    # if method == 'GET':
    #     htmlcode = request.data
    #     return render_template('result.html', htmlcode=htmlcode)

    # 從 session 中取得 uid TODO:
    uid = 'user_001'
    host_name = current_app.config['BASE_URL']

    # 取得 POST 的收件人資訊 TODO:
    trade_name = 'amos'
    trade_phone = '12345678'
    county = '台北市'
    district = '中正區'
    zipcode = '100'
    address = '中華路一段1號'

    # 利用 uid 查詢資料庫，購物車商品 & 價錢
    cart = json.loads(request.form['cart'])
    products = json.loads(request.form['products'])

    total_product_price = 0
    total_product_name = ''

    for i in cart:
        price = products[i]['price']
        quantity = cart[i]
        product_name = products[i]['name']
        total_product_price += price * int(quantity)
        total_product_name += product_name + '#'

    # 建立交易編號 tid
    date = time.time()
    tid = str(date) + 'Uid' + str(uid)
    status = '未刷卡'

    # 新增 Transaction 訂單資料 TODO:



    # 設定傳送給綠界參數
    order_params = {
        'MerchantTradeNo': datetime.now().strftime("NO%Y%m%d%H%M%S"), # *購物平台自訂交易編號 string(20)
        'StoreID': '', # 可填分店代號 string(20)
        'MerchantTradeDate': datetime.now().strftime("%Y/%m/%d %H:%M:%S"), # *交易時間 yyyy/MM/dd HH:mm:ss
        'PaymentType': 'aio', # *交易類型 string(20) 
        'TotalAmount': total_product_price, # *交易金額 int
        'TradeDesc': 'test by amos', # 交易描述，自行輸入 string(200)
        'ItemName': total_product_name, # *商品名稱，自行輸入 string(400) 手機20元x2#隨身碟60元x1
        
        # 'ReturnURL': host_name + 'payment/receive_result', # *購物平台 付款完成返回結果/
        'ReturnURL': host_name + '/payment/back_from_ecpay',

        'ChoosePayment': 'Credit', # *選擇付款方式 All, Credit, WebATM, ATM, CVS, BARCODE
        'ClientBackURL': host_name + '/',
        'Remark': '交易備註aaaa', # 交易備註，自行輸入 string(100)
        'ChooseSubPayment': '', # 付款子項目 string(20)
        'OrderResultURL': host_name + '/payment/trad_result', # 購物平台 付款結果頁 string(200)
        'NeedExtraPaidInfo': 'Y',
        'DeviceSource': '',
        'IgnorePayment': '',
        'PlatformID': '',
        'InvoiceMark': 'N',
        'CustomField1': str(tid),
        'CustomField2': '',
        'CustomField3': '',
        'CustomField4': '',
        'EncryptType': 1,
    }

    extend_params_1 = {
        'BindingCard': 0,
        'MerchantMemberID': '',
    }

    extend_params_2 = {
        'Redeem': 'N',
        'UnionPay': 0,
    }

    inv_params = {
        # 'RelateNumber': 'Tea0001', # 特店自訂編號
        # 'CustomerID': 'TEA_0000001', # 客戶編號
        # 'CustomerIdentifier': '53348111', # 統一編號
        # 'CustomerName': '客戶名稱',
        # 'CustomerAddr': '客戶地址',
        # 'CustomerPhone': '0912345678', # 客戶手機號碼
        # 'CustomerEmail': 'abc@ecpay.com.tw',
        # 'ClearanceMark': '2', # 通關方式
        # 'TaxType': '1', # 課稅類別
        # 'CarruerType': '', # 載具類別
        # 'CarruerNum': '', # 載具編號
        # 'Donation': '1', # 捐贈註記
        # 'LoveCode': '168001', # 捐贈碼
        # 'Print': '1',
        # 'InvoiceItemName': '測試商品1|測試商品2',
        # 'InvoiceItemCount': '2|3',
        # 'InvoiceItemWord': '個|包',
        # 'InvoiceItemPrice': '35|10',
        # 'InvoiceItemTaxType': '1|1',
        # 'InvoiceRemark': '測試商品1的說明|測試商品2的說明',
        # 'DelayDay': '0', # 延遲天數
        # 'InvType': '07', # 字軌類別
    }

    # 合併延伸參數
    order_params.update(extend_params_1)
    order_params.update(extend_params_2)

    # 合併發票參數
    order_params.update(inv_params)

    module = import_ecpay_module()

    # TODO: 之後改由current_app.config['MerchantID']讀取設定
    ecpay_payment_sdk = module.ECPayPaymentSdk(MerchantID='2000132',
                                               HashKey='5294y06JbISpM5x9',
                                               HashIV='v77hoKGq4kWxNNIS')

    try:
        # 產生綠界訂單所需參數
        final_order_params = ecpay_payment_sdk.create_order(order_params)

        # 產生 html 的 form 格式
        action_url = 'https://payment-stage.ecpay.com.tw/Cashier/AioCheckOut/V5'
        htmlcode = ecpay_payment_sdk.gen_html_post_form(action_url, final_order_params)
        # print(render_template('pay.html', htmlcode=htmlcode))
        # return render_template('pay.html', htmlcode=htmlcode)
        return htmlcode
    except Exception as error:
        print('錯誤: ' + str(error))


# 回到商店
@payment.route('/back_from_ecpay', methods=['GET', 'POST'])
def back_from_ecpay():
    if request.method == 'GET':
        print('get  back from ecpay.....')
        return '<body>get ok</body>'

    if request.method == 'POST':
        print('post  back from ecpay.....')
        return '<body>post ok</body>'
        


# ReturnURL: 綠界 Server 端回傳 (POST)
# @csrf.exempt
# @payment.route('/receive_result', methods=['POST'])
# def end_return():
#     result = request.form['RtnMsg']
#     tid = request.form['CustomField1']
#     print(result, tid)

#     # TODO:
#     # trade_detail = sql.Transaction.query.filter_by(tid=tid).first()
#     # trade_detail.status = '交易成功 sever post'
#     # db.session.add(trade_detail)
#     # db.session.commit()

#     return '1|OK'


# OrderResultURL: 綠界 Client 端 (POST)
@csrf.exempt
@payment.route('/trad_result', methods=['GET', 'POST'])
def end_page():
    if request.method == 'GET':
        return redirect(url_for('index'))

    if request.method == 'POST':
        return '<div>恭喜付款成功</div>'