import os
from datetime import timedelta

class Config(object):
    ROOT_DIR = os.path.dirname(os.path.realpath(__file__))
    # BASE_URL = 'localhost'
    BASE_URL = 'https://031f-2001-b400-e25d-2f05-ef62-c8b9-393b-cb78.ngrok.io' # TODO:
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = os.getenv('SECRET_KEY', b'dckflrovjdyjfkgog/c,vkfkjddjekg13okzmck')

class ProductConfig(Config):
    DEBUG = False
    TESTING = False

    # 資料庫
    SQLALCHEMY_DATABASE_URI = 'sqlite:///static/sampleShop.db'
    # MYSQL_HOST = 'mysql'
    # MYSQL_USER = 'account'
    # MYSQL_PASSWORD = 'account0982'
    # MYSQL_DB = 'account'
    # SQLALCHEMY_DATABASE_URI = f'mysql+pymysql://{MYSQL_USER}:{MYSQL_PASSWORD}@{MYSQL_HOST}/{MYSQL_DB}'
    # SQLALCHEMY_TRACK_MODIFICATIONS = False

    # 金流
    MerchantID='', # *綠界提供的特店編號 string(10)
    HashKey='', # *加密設定
    HashIV='' # *加密設定
    action_url = 'https://payment.ecpay.com.tw/Cashier/AioCheckOut/V5'

    # 寄信
    # Flask-Mail.
    # MAIL_DEFAULT_SENDER = 'wootu.test@gmail.com'  # 要修改！
    # MAIL_SERVER = 'smtp.gmail.com'
    # MAIL_PORT = 587
    # MAIL_USE_TLS = True
    # MAIL_USE_SSL = False
    # MAIL_USERNAME = 'wootu.test@gmail.com'  # 要修改！
    # MAIL_PASSWORD = ''  # 要修改！


class DevelopmentConfig(Config):
    DEBUG = True
    TESTING = False

    # 資料庫
    SQLALCHEMY_DATABASE_URI = 'sqlite:///static/sampleShop.db'
    # MYSQL_HOST = 'mysql'
    # MYSQL_USER = 'account'
    # MYSQL_PASSWORD = 'account0982'
    # MYSQL_DB = 'account'
    # SQLALCHEMY_DATABASE_URI = f'mysql+pymysql://{MYSQL_USER}:{MYSQL_PASSWORD}@{MYSQL_HOST}/{MYSQL_DB}'
    # SQLALCHEMY_TRACK_MODIFICATIONS = True
    # SQLALCHEMY_ECHO = True

    # 金流
    MerchantID = '2000132', # *綠界提供的特店編號 string(10)
    HashKey = '5294y06JbISpM5x9', # *
    HashIV = 'v77hoKGq4kWxNNIS' # *
    action_url = 'https://payment-stage.ecpay.com.tw/Cashier/AioCheckOut/V5'

    # 寄信
    # Flask-Mail.
    # MAIL_DEFAULT_SENDER = 'wootu.test@gmail.com'  # 要修改！
    # MAIL_SERVER = 'smtp.gmail.com'
    # MAIL_PORT = 587
    # MAIL_USE_TLS = True
    # MAIL_USE_SSL = False
    # MAIL_USERNAME = 'wootu.test@gmail.com'  # 要修改！
    # MAIL_PASSWORD = ''  # 要修改！